--[[

Copyright © 2016 F. Georget <fabien.georget@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

--]]

minetest.register_craftitem("alchemy:tube", {
	description = "Empty tube",
	inventory_image = "alchemy_empty_tube.png",
})

minetest.register_craftitem("alchemy:potion", {
	description = "Health potion",
	inventory_image = "alchemy_potion_health.png",
	on_use = minetest.item_eat(10)
})

minetest.register_craftitem("alchemy:breath_potion", {
	description = "Breathing potion",
	inventory_image = "alchemy_potion_breath.png",
	on_use = function(itemstack, user, pointedthing)
		user:set_breath(10)
		itemstack:take_item(1)
		return itemstack
	end
})

minetest.register_craftitem("alchemy:poison", {
	description = "Poison",
	inventory_image = "alchemy_potion_poison.png",
	on_use = function(itemstack, user, pointedthing)
		itemstack:take_item(1)
		user:set_hp(0)
		return itemstack
	end
})
minetest.register_craftitem("alchemy:poisoned_apple", {
	description = "Smelly apple",
	inventory_image = "default_apple.png",
	on_use = function(itemstack, user, pointedthing)
		itemstack:take_item(1)
		user:set_hp(user:get_hp()-10)
		return itemstack
	end
})


minetest.register_craft({
	output = "alchemy:tube 15",
	recipe = {
		{"default:glass"},
		{"default:glass"},
		{"default:glass"}
		}	
})

minetest.register_craft({
	type = "shapeless",
	output = "alchemy:poisoned_apple",
	recipe = {"default:apple", "alchemy:poison"}
})

