--[[

Copyright © 2016 F. Georget <fabien.georget@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

--]]

local modpath = minetest.get_modpath("alchemy")
dofile(modpath.."/powders.lua")
dofile(modpath.."/potions.lua")
dofile(modpath.."/grinder.lua")
dofile(modpath.."/mixer.lua")
