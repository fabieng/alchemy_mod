Alchemy
=======

Alchemy is a Minetest mod. It provides the basic tools to set up an alchemy lab.

# Machines #

The machines are nodes who transform reactants into products. Two machines are currently defined.

## Powder grinder ##

The mortar grind the reactants into powders, These powders are used as reactants in the other machines.

The grinder is crafted following this recipe : 

~~~~
{
	{steel ingot, steel ingot, steel ingot}
	{steel ingot, diamond    , steel ingot}
	{cobblestone, cobblestone, cobblestone}
}
~~~~


## Potion mixer ##

This machine mixes two powders into a solution. To be successful, an empty tube and a bucket of water must also be available. Alchemy is a complex and difficult art ! The process is not always successful. The potion mixer yields 2, 1 (most common) or 0 potion for each try. In rare cases, the reaction gets out of control and explodes ! The explosion damages the users near the machine. A secret catalyser can help to avoid the explosion. But if the wrong catalyser is used, it will increase the damage !

The mixer is crafted following this recipe : 

~~~~
{
	{copper ingot, glass      , copper ingot}
	{glass       , glass      , glass}
	{cobblestone , cobblestone, cobblestone}
}
~~~~


# Powders #

The powders are secondary products used to make the potion. They are obtained by grinding other craftitems. The following recipes are accepted :

- Blue geranium : blue powder
- Rose : red powder
- Grass : green powder
- Red mushroom : firebrick powder

# Potion #

The potions are mixed in the potion mixer. The following recipes are accepted :

- red + white powder : health potion (+10 hp)
- blue + white powder : breath potion (restore breath)
- firebrick + blue powder : poison (instant kill)


# Items #

Currently, the mod define only one item : the smelly apple. It is crafted using a poison potion and an apple. Eating the smelly apple removes 10 hp.


# About #

The code is under the license WFTPL. Patches and ideas are welcomed.

# Todo #

- Transmustation device to transform coal into gold, and copper into diamond.
- Better textures (Contributions are welcome !)
- more recipes

