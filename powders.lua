--[[

Copyright © 2016 F. Georget <fabien.georget@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

--]]

-- powders : mix powders into solution

-- the list of available powders
local powder_colors = {"red", "blue", "white", "firebrick", "green"}

-- Register the powders
for _, color in pairs(powder_colors) do
	minetest.register_craftitem("alchemy:powder_"..color, {
		description = color:gsub("^%l", string.upper).." powder",
		inventory_image = "alchemy_powder_white.png^[colorize:"..color..":150"
	})
end

-- Return true if stackname is a powder
function is_powder(stackname)
	for _, c in pairs(powder_colors) do
		if stackname == "alchemy:powder_"..c
		then
			return true
		end
	end
	return false
end
